import LoginPage from  '../pageobjects/login.page';
import LandingPage from '../PageObjects/Landing.Page';
import HomePage from '../PageObjects/HomePage';
import Utilities from '../Common/Utilities';
import config from '../config';
const expectChai = require('chai').expect

describe('Sciensus app login tests', () => {
    
    it('should login with valid credentials', async () => {
        
        /**
         * Apply implicit wait to the test
         */
        await browser.setImplicitTimeout(20000)
        
        /**
         * click on Login link on landing page
         */
        LandingPage.clickOnLoginLink();
        console.log('Clicked on Login Link')
        
        /**
         * switch from Native View  to Web View
         */
        Utilities.switchView('webview')
        console.log('switched to Web View')
        
        /**
         * Enter username, password and click on submit button
         */
        await LoginPage.login(config.username, config.password);
        console.log('Successfully entered credentials and submittted')

        /**
         * Switch back to Native View
         */
        Utilities.switchView('nativeview')
        console.log('Switched back to native view')

        /**
         * Verify user reached home screen
         */
         expectChai(HomePage.howCanWeHelpText).to.equal(true);
         console.log('Successfully verified Home Page text')
    
    });
});


