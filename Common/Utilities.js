
class Utilities {

    
    async switchView (view) {
        const contexts = await driver.getContexts()
        console.log('All contexts: ', contexts)
        if (view === 'webview') {
            await driver.switchContext('WEBVIEW_chrome')
        }
        else
        await driver.switchContext('NATIVE_APP')
    }

}

export default new Utilities();