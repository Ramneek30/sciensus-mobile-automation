const HOW_CAN_WE_HELP_TEXT = '//android.widget.TextView[@text="How can we help?"]'

class HomePage {
    /**
     * define selectors using getter methods
     */
    get howCanWeHelpText() {
        return $(HOW_CAN_WE_HELP_TEXT);
    }

    /**
     * a method to encapsule automation code to interact with the page
     */
    async verifyHeaderTextIsDisplayed () {
        return this.howCanWeHelpText.isDisplayed;
    }

}

export default new HomePage();