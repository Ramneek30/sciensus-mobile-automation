const LOGIN_LINK = '//android.widget.TextView[@text="Continue to login"]'
const LOGIN_LINK_UISELECTOR = 'new UiScrollable(new UiSelector().scrollable(true)).scrollIntoView(new UiSelector().text(\"Continue to login\"))'

class LandingPage {
    /**
     * define selectors using getter methods
     */
    get loginLink() {
        return $(LOGIN_LINK);
    }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async clickOnLoginLink () {
        await $(`android=${LOGIN_LINK_UISELECTOR}`)
        await this.loginLink.click();
    }

}

export default new LandingPage();